package org.example.ovn1;

public class Main {

    // Om du vill köra kod / övningar utan att använda automattester
    public static void main(String[] args) {

    }

    // Om du vill lösa uppgifterna med automatiserad unittestning / "rättning"

    // Uppgift 1
    public static int createAnIntWithValue5(){

        // TODO: Skapa en varibel av typen int och med värdet 5
        int number = 10;

        // TODO: ersätt '0' med ditt variabelnamn
        return number;
    }

    // Uppgift 2
    public static boolean createABooleanThatIsTrue(){

        // TODO: Skapa en varibel av typen boolean och med värdet true

        // TODO: ersätt 'false' med ditt variabelnamn
        return false;
    }

    public static String createAName(){

        // TODO: Skapa en varibel som håller namnet "Gunnar"

        // TODO: ersätt "placeholder" med ditt variabelnamn
        return "placeholder";

    }

    // TODO: Uncomment and fill in the rest of the below method per instructions
    // The method should be named "add"
    // The method should allow passing two arguments of type int
    // The method should return the sum of the two arguments, in the form of an int
    /*
    public static ... {
         ...
    }
    */

    // TODO: Create a class 'Dog', so that the code compiles and behaves as expected
    // The dog should have a name, an age, a constructor and getter / setter methods
    // TODO: Uncomment the following block of code
    /*
    public static Dog supplyADog(int age, String name){
        return new Dog(age, name);
    }
    */

    // TODO: Fill in the rest of the method, so that it give the supplied dog the new name
    // TODO: Uncomment the following block of code
    /*
    public static void changeDogName(Dog dog, String newName){

    }
    */

    // TODO: Adjust the method, so that it returns an all-uppercase version of supplied name
    // Hint: String är en klass med färdiga metoder, se om du hittar någon lämplig
    public static String makeNameUppercase(String name){
        return name;
    }

    // TODO: Bonusuppgiften - byt värde på x och y innan de returneras
    public static int[] switchValues(int x, int y){

        // TODO: Skriv kod här som switchar värdena på x och y med varandra;
        // TODO: Svårare version, byt värdena utan att skapa någon tredje variabel

        return new int[]{x, y};
    }




}
