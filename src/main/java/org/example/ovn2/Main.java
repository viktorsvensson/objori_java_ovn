package org.example.ovn2;

public class Main {


    // Om du vill köra kod / övningar utan att använda automattester
    public static void main(String[] args) {

    }

    // Om du vill lösa uppgifterna med automatiserad unittestning / "rättning"

    // Uppgift 1
    public static Animal createAnAnimal(){

        // TODO: Skapa en klass 'Animal'
        // Klassen skall ha...
        // Ett första fält "age" av typen int,
        // Ett andra fält "humanAgeComparisonFactor" av typen int
        // Ett tredje fält av typen "artName"
        // Getters för alla fälten
        // En statisk metod som tar in ett object av typen Animal som argument,
        // och som returnerar djurets ålder i motsvarande människoår
        // (beräknat m.h.a. "humanAgeComparisonFactor")



        // return new Animal(12, 7, "Varg"); // TODO: Uncomment
        return null; // <-- remove line;
    }





    // Uppgift 2

    public static Carnivore createACarnivore(){

        // TODO: Skapa en klass 'Carnivore'
        // Klassen skall...
        // Extenda / ärva klassen Animal
        // Ha en ett fält "killcount" av typen int med tillhörande getter-metod


        // return new Carnivore(12, 7, "Varg", 0); // TODO: Uncomment
        return null; // <-- remove line
    }


    // Uppgift 3
    public static Herbivore createAHerbivore(){

        // TODO: Skapa en klass 'Herbivore'
        // Klassen skall...
        // Extenda / ärva klassen Animal
        // Ha en metod "getFavoriteFood". Ingen retur, inga parametrar, valfri metodkropp


        //return new Herbivore(12, 15, "Kanin"); // TODO: Uncomment
        return null; // <-- remove
    }


    // Uppgift 4

    public static Animal[] returnAnimalArray(){

        // TODO: Returnera en array med två Carnivore och två Herbivore

        return null; // TODO: replace 'null' with array
    }

    // Uppgift 7
    public static String[] returnAStringArray(){

        // TODO: Skapa en array som håller String-object. Arrayen skall ha storlek 10


        return null; // TODO: replace 'null' with array
    }

    // Uppgift 8
    public static double[] returnADoublesArray(){

        // TODO: Skapa en doubles-array som håller värdena 3.1, 3.26, 7.12, 6.13


        return null; // TODO: replace 'null' with array
    }

    // Uppgift 9
    // TODO: Uncomment and fill in the rest of the below method per instructions
    // The method should be named "findFirstPrime"
    // The method should allow passing one argument of type int[]
    // The method should return the first prime number it finds within the array (as an int)
    /*
    public static ... {
         ...
    }
    */

    // Uppgift 10
    // TODO: Uncomment and fill in the rest of the below method per instructions
    // The method should be named "shortenTheWords"
    // The method should allow passing one argument of type String[]
    // The method should return the array, but with all strings within sliced to only their first 3 characters
    /*
    public static ... {
         ...
    }
    */

    // Uppgift 11 - bonusuppgift
    // TODO: Uncomment and fill in the rest of the below method per instructions
    // The method should be named "wordplay"
    // The method should allow passing one argument of type String[]
    // The method should return the array, but..
    // All the words containing an even number of characters,
    // should be replaced by their number of characters represented as roman numerals (of type string)
    // All the words containing an odd number of characters,
    // should be replaced by their number of characters represented in binary numbers (but still as a string)
    // "hatt" -> "IV", "Mamma" -> "101".

    /*
    public static ... {
         ...
    }
    */



}
