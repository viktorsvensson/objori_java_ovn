package org.example.ovn3;

import java.util.List;

public class Main {

    // Om du vill köra kod / övningar utan att använda automattester
    public static void main(String[] args) {

    }

    // Om du vill lösa uppgifterna med automatiserad unittestning / "rättning"
    // Kommentera ut sådana uppgifter som ej låter koden kompilera

    // Uppgift 1
    /*
    public static Creature createACreature(){

        // TODO: Skapa en klass 'Creature' som implementerar ett interface "Predator"
        // Klassen Creature skall ha en ett fält av typen String, "killingSound";
        // interfacet skall ha en metod 'kill', returtyp String, inga parametrar
        // Creature skall implementera 'kill', genom att returnera dess killingSound

        return null; // Returnera din Creature istället
    }
    */


    // Uppgift 2
    /*
    public static Predator createAPredator(){

        // TODO: Implementera Predator genom en anonym innerklass

        return null; // Returnera din Predator iställlet
    }
    */

    // Uppgift 3
    public static List<String> createAList(){

        // TODO: Skapa en lista med 3 Textsträngar

        return null; // <-- Returnera din lista istället
    }


    // Uppgift 4

    public static List<String> removeSecondWord(List<String> list){

        // TODO: Ta bort det andra ordet i listan

        return list;
    }

    // Uppgift 5
    // TODO: Skapa en metod som returnerar det första ordet i listan som är ett palindrom
    // The method should be named "findFirstPalindrome"
    // The method should allow passing one argument of type List<String>
    // The method should return, as a String, the first palindrome in the list
    public static String findFirstPalindrome(List<String> strings) {
        return null;
    }

    // Uppgift 6.
    // Skriv kod som som tvingar fram ett exception
    public static void forceException() {

    }


    // Uppgift 7.
    // Skapa en ett eget checked exception 'IllegalAlcoholLevelException' som kan ta in ett message



}
