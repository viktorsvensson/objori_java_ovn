package org.example.ovn4;

import org.example.ovn3.Creature;
import org.example.ovn3.Predator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    // Om du vill köra kod / övningar utan att använda automattester
    public static void main(String[] args) {

    }

    // Om du vill lösa uppgifterna med automatiserad unittestning / "rättning"
    // Kommentera ut sådana uppgifter som ej låter koden kompilera
    // Om du vill kan du manuellt testköra dina statiska uppgiftsmetoder i public static void main-metoden.

    // Uppgift 1

    public static String readAName(){

        // Use the scanner class to read a user provided name from the terminal

        return null; // Return the name
    }



    // Uppgift 2
    // Något går fel här när vi ska läsa in favoritmaten
    // Testkör den här metoden i main-metoden ovan med debuggern och försök lista ut problemet
    // fixa i metoden så att favoriteFood också kan läsas in korrekt
    public static String readFoodAfterHavingReadAge(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Vad är ditt namn?");
        String name = scanner.nextLine();
        System.out.println("Hej " + name);
        System.out.println("Vad är din ålder?");
        int age = scanner.nextInt();
        System.out.println("Din ålder är: " + age);
        System.out.println("Vad är din favoriträtt?");
        String favoriteFood = scanner.nextLine();
        System.out.println("Din favoritmat är " + favoriteFood);

        return favoriteFood;
    }

    // Uppgift 3
    public static int readAnInteger(){
        // ersätt värdet på age med ett heltal som användaren matat in.
        // om ett icke-giltigt värde matas in (bokstäver) be användaren försöka igen tills korrekt

        int number = 0;


        return number;
    }

    // Uppgift 4 - svårare, bonusuppgift
    public static int readAValidAge(){
        // ersätt värdet på age med en åldern som användaren matat in.
        // Åldern får inte vara ett negativt värde
        // om ett icke-giltigt värde matas in (bokstäver, negativt nummer) be användaren försöka igen tills korrekt
        int age = -1;

        return age;
    }


    // Uppgift 5
    // Arbeta testdrivet fram en Calculator med metoder för addition, subtraktion, multiplikation och division
    // Du bestämmer själv de närmare kraven på respektive metod (vilka datatyper de använder, hur de hantera t.ex. division m. 0 etc.)
    // Du skapar själv först unittestet för respektive metod - sedan implementerar du metoden. Rinse & Repeat :)



}
