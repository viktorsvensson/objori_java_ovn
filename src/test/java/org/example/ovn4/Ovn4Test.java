package org.example.ovn4;

import org.example.ovn3.Creature;
import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Ovn4Test {

    private static final InputStream CONSOLE = System.in;

    @AfterEach
    public void cleanUp(){
        System.setIn(CONSOLE);
    }

    @Test
    @DisplayName("1.")
    @Order(1)
    public void readAName(){
        // When
        System.setIn(new ByteArrayInputStream("Mats".getBytes()));
        String name = Main.readAName();


        // Then
        assertEquals("Mats", name);
    }

    @Test
    @DisplayName("2.")
    @Order(2)
    public void readANameAgeAndFood(){
        // When
        System.setIn(new ByteArrayInputStream("Mats\n12\nPasta\n".getBytes()));
        String food = Main.readFoodAfterHavingReadAge();

        // Then
        assertEquals("Pasta", food);
    }

    @Test
    @DisplayName("4.")
    @Order(4)
    public void readAnInteger(){
        // When
        System.setIn(new ByteArrayInputStream("Mats\n12\n".getBytes()));
        int number = Main.readAnInteger();

        // Then
        assertEquals(12, number);
    }

    @Test
    @DisplayName("4.")
    @Order(4)
    public void readAValidAge(){
        // When
        System.setIn(new ByteArrayInputStream("-7\n12\n".getBytes()));
        int number = Main.readAValidAge();

        // Then
        assertEquals(12, number);
    }

    // Här kan du fylla på med dina egna unittester för din Calculator-uppgift
    // Alt. skapa en egen klass CalculatorTest med testerna.

}
