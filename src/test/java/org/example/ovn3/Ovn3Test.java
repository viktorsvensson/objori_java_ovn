package org.example.ovn3;

import org.example.ovn2.Animal;
import org.example.ovn2.Carnivore;
import org.example.ovn2.Herbivore;
import org.example.ovn3.Main;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Ovn3Test {

    // OBS! Ändra inte i testerna annat än att för att uncomment där så anges
    // Ändra istället i huvudkoden tills testerna blir gröna

    @Test
    @DisplayName("1. Skapa en 'Creature' enligt instruktionerna")
    @Order(1)
    public void createAnCreature(){
        // When
        // TODO: uncomment
        /*
        Creature creature = new Creature("Rawr!");

        // Then
        //assertEquals("Rawr!", creature.kill());
        */

    }

    @Test
    @DisplayName("2. Skapa en implementation av Predator enligt instruktionerna")
    @Order(2)
    public void createAPredator(){
        // When
        assertDoesNotThrow(() -> {
            // Predator predator = Main.createAPredator(); // TODO: Uncomment
        });
    }

    @Test
    @DisplayName("3. Skapa en lista enligt instruktionerna")
    @Order(3)
    public void createAList(){
        // When
        List<String> list = Main.createAList();

        // Then
        assertEquals(3, list.size());
    }

    @Test
    @DisplayName("4. Ta bort det andra ordet i listan")
    @Order(4)
    public void removeSecondWord(){
        // Given
        List<String> origin = new ArrayList<>(List.of("Alice", "Bob", "Charles"));
        List<String> expected = new ArrayList<>(List.of("Alice", "Charles"));

        // When
        List<String> result = Main.removeSecondWord(origin);

        // Then
        assertIterableEquals(expected, result);
    }



    @Test
    @DisplayName("5. findFirstPalindrome")
    @Order(5)
    public void findFirstPalindrome(){
        // Given
        List<String> names = List.of("alice", "agda", "anna", "alva");

        // When
        // TODO: uncomment:

        String result = Main.findFirstPalindrome(names);

        //Then
        assertEquals("anna", result);

    }

    @Test
    @DisplayName("6. forcera ett valfritt runtimeexception")
    @Order(6)
    public void forceRuntimeException(){

        assertThrows(RuntimeException.class, Main::forceException);

    }

    @Test
    @DisplayName("7. Skapa ett eget exception")
    @Order(7)
    public void createAlcholException(){

        // TODO: uncomment:
        /*
        IllegalAlcoholLevelException e = new IllegalAlcoholLevelException("Här var det ett felmeddelande")
        assertNotNull(e);
        assertEquals("Här var det ett felmeddelande", e.getMessage());
        */

    }

}
