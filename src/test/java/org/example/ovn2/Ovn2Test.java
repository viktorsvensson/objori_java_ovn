package org.example.ovn2;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Ovn2Test {

    // OBS! Ändra inte i testerna annat än att för att uncomment där så anges
    // Ändra istället i huvudkoden tills testerna blir gröna

    @Test
    @DisplayName("1. Skapa en klass animal enligt instruktionerna")
    @Order(1)
    public void createAnAnimal(){
        // When
        Animal animal = Main.createAnAnimal();

        // Then
        // TODO: uncomment
        /*
        assertEquals(12, animal.getAge());
        assertEquals(7, animal.getHumanAgeComparisonFactor());
        assertEquals("Varg", animal.getArtName());
        assertEquals(84, Animal.getHumanEquivalentYears(animal));
        */
    }

    @Test
    @DisplayName("2. Skapa en klass Carnivore enligt instruktionerna")
    @Order(2)
    public void createACarnivore(){
        // When
        Carnivore carnivore = Main.createACarnivore();

        // Then
        // assertEquals(0, carnivore.getKillcount()); // TODO: uncomment
    }

    @Test
    @DisplayName("3. Skapa en klass Herbivore enligt instruktionerna")
    @Order(3)
    public void createAnHerbivore(){
        // When
        Herbivore herbivore = Main.createAHerbivore();

        // Then
        assertNotNull(herbivore);
    }

    @Test
    @DisplayName("4. Skapa en lista med Carnivore och Herbivore enligt instruktionerna")
    @Order(4)
    public void returnAnAnimalArray(){
        // When
        Animal[] animals = Main.returnAnimalArray();

        // Then
        // TODO: uncomment
        /*
        assertTrue(Arrays.stream(animals).anyMatch(animal -> animal instanceof Herbivore));
        assertTrue(Arrays.stream(animals).anyMatch(animal -> animal instanceof Carnivore));
         */
    }


    @Test
    @DisplayName("7. returnera en array enligt instruktionerna")
    @Order(7)
    public void returnAStringArray(){
        // When
        String[] strings = Main.returnAStringArray();

        // Then
        assertEquals(10, strings.length);
    }

    @Test
    @DisplayName("8. Returnera en array av doubles enligt instruktionerna")
    @Order(8)
    public void returnADoublesArray(){
        // When
        double[] result = Main.returnADoublesArray();
        double[] expected = {3.1, 3.26, 7.12, 6.13};

        // Then
        assertArrayEquals(expected, result);
    }


    @Test
    @DisplayName("9. findFirstPrime")
    @Order(9)
    public void findFirstPrime(){
        // Given
        int[] ints = {4, 9, 12, 7, 8, 20, 17};

        // When
        // TODO: uncomment:
        /*
        int result = Main.findFirstPrime(ints);

        //Then
        assertEquals(7, result);
        */
    }

    @Test
    @DisplayName("10 shortenTheWords")
    @Order(10)
    public void shortenTheWords(){
        //Given
        String[] strings = {"Arboga", "Ekvator", "Brandbil"};
        String[] expected = {"Arb", "Ekv", "Bra"};

        // When
        // TODO: uncomment:
        /*
        String[] result = Main.shortenTheWords(strings);

        // Then
        assertArrayEquals(expected, result);
        */
    }

    @Test
    @DisplayName("11. Bonus - wordplay")
    @Order(11)
    public void wordplay(){
        String[] strings = {"Schampoo", "Skruv"};
        String[] expected = {"VIII", "101"};

        // When
        // TODO: uncomment:
        /*
        String[] result = Main.wordplay(strings);

        // Then
        assertEquals(expected, result);
        */
    }

}
