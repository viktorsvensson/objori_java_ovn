package org.example.ovn1;

import org.junit.jupiter.api.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Ovn1Test {

    // OBS! Ändra inte i testerna annat än att för att uncomment där så anges
    // Ändra istället i huvudkoden tills testerna blir gröna

    @Test
    @DisplayName("1. Skapa och returnera en variabel av typ int, värde 5")
    @Order(1)
    public void createAnInt(){

        // When
        int result = Main.createAnIntWithValue5();

        // Then
        Assertions.assertEquals(5, result);
    }

    @Test
    @DisplayName("2. Skapa och returnera en variabel av typ boolean, värde true")
    @Order(2)
    public void createABooleanThatIsTrue(){

        // When
        boolean result = Main.createABooleanThatIsTrue();

        // Then
        Assertions.assertTrue(result);
    }

    @Test
    @DisplayName("3. Skapa och returnera en variabel som håller ett namn, värde 'Gunnar'")
    @Order(3)
    public void createAName(){

        // When
        String result = Main.createAName();

        // Then
        Assertions.assertEquals("Gunnar", result);
    }

    @Test
    @DisplayName("4. Skapa metoden add, som tar in två tal och returnerar summan")
    @Order(4)
    public void add(){

        // When
        // TODO: Uncomment the following block
        /*
        int result = Main.add(1, 3);

        // Then
        Assertions.assertEquals(4, result);
         */
    }

    @Test
    @DisplayName("5. Skapa en class 'Dog' enligt instruktioner i uppgift")
    @Order(5)
    public void supplyADog(){

        // When
        // TODO: Uncomment the following block
        /*
        Dog resultingDog = Main.supplyADog(12, "Fido");

        // Then
        Assertions.assertEquals(12, resultingDog.getAge());
        Assertions.assertEquals("Fido", resultingDog.getName());
        */
    }

    @Test
    @DisplayName("6. Fyll i metoden enligt instruktioner så att hunden byter namn")
    @Order(6)
    public void changeDogName(){

        // When
        // TODO: Uncomment the following block
        /*
        Dog dog = new Dog(12, "Fido");
        Main.changeDogName(dog, "Gunnar");

        // Then
        Assertions.assertEquals("Gunnar", dog.getName());
        */
    }

    @Test
    @DisplayName("7. Fyll i metoden enligt instruktioner så det returnerade namnet är Uppercase")
    @Order(7)
    public void nameToUppercase(){

        // When
        String name = "Gunnar";
        String returnedName = Main.makeNameUppercase(name);

        // Then
        Assertions.assertEquals("GUNNAR", returnedName);
    }

    @Test
    @DisplayName("8. (Bonus) Byt plats på värdena")
    @Order(8)
    public void switchValues(){

        // When
        int[] results = Main.switchValues(10, 5);

        // Then
        Assertions.assertEquals(5, results[0]);
        Assertions.assertEquals(10, results[1]);
    }

}
